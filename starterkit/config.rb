require 'autoprefixer-rails'
require 'sass-globbing'
require 'fileutils'


# Set this to the root of your project when deployed:
http_path = "../"
css_dir = "css"
sass_dir = "scss"
images_dir = "images"
javascripts_dir = "js"
fonts_dir = "fonts"

output_style = :expanded
line_comments = false

on_stylesheet_saved do |file|
  css = File.read(file)
  map = file + '.map'

  if File.exists? map
    result = AutoprefixerRails.process(css,
      from: file,
      to:   file,
      map:  { prev: File.read(map), inline: false })
    File.open(file, 'w') { |io| io << result.css }
    File.open(map,  'w') { |io| io << result.map }
  else
    File.open(file, 'w') { |io| io << AutoprefixerRails.process(css, browsers: ['last 3 versions']) }
  end

  # Move files to error 'theme'
  file_object = File.open(file)
  file_contents = file_object.read

  if File.basename(file) == 'bootstrap.css'
    # Bootstrap can be moved like original
    filename = File.basename(file)
  else
    # Theme file itself needs to be renamed
    filename = 'error.css'
    themename = File.basename(file, ".*")

    # Is there a config entry for the logo image?
    config = File.join(File.dirname(__FILE__), '../../../../config/default/' + themename + '.settings.yml')
    if File.file?(config)
      config_file = File.open(config)
      regex = config_file.read.match(/logo:\n  path: (.*)$/)

      if regex[1] != nil
        # Config entry found, we replace the logo palceholder
        file_contents = file_contents.gsub('__logo-placeholder__', '/' + regex[1]);
      end
    end
  end

  # Write the file to the error theme
  error_directory = File.join(File.dirname(__FILE__), '../error')
  unless File.directory?(error_directory)
    FileUtils.mkdir_p(error_directory)
  end
  File.open(error_directory + '/' + filename, 'w'){ |io| io.write file_contents }
  File.open(file, 'w'){ |io| io.write file_contents }
end
