(function($, window, document, Drupal) {

	// USE FOR TESTING PURPOSE ONLY
	// var Cookiebot = {};
	// Cookiebot.consent = {};
	// Cookiebot.consent.statistics = 'true';
	// Cookiebot.consent.marketing = 'true';

	function reloadVideos() {
		if (typeof(Cookiebot) !== "undefined" && typeof(Cookiebot.consent) !== "undefined" && Cookiebot.consent.marketing) {
			$('.video-embed-field-responsive-video, .field--name-field-media-oembed-video').each(function() {
				$(this).find('iframe').attr('src', $(this).find('iframe').data('src'));
				$(this).find('.no-cookies').remove();
			});

			$('a[class*="video"][data-content]').each(function() {
				$(this).attr('data-content', $(this).data('content').replace(/data-|<p class="no-cookies">(.*?)<\/p>/g, ''));
			});
		}
	}
	$(window).on('CookiebotOnAccept', function(event) {
		reloadVideos();
	});
})(jQuery, window, document, Drupal);