(function($, window, document, Drupal) {

	$.fn.isInViewport = function(offset) {
		if($(this).length) {
			var elementTop = $(this).offset().top;
			var elementBottom = elementTop + $(this).outerHeight();

			var viewportTop = $(window).scrollTop();
			var viewportBottom = viewportTop + $(window).height();

			return elementBottom > viewportTop && elementTop < viewportBottom - (($(window).height() / 100) * offset);
		} else {
			return false;
		}
	};

	function LazyLoad(offset) {
		this.scrollTimer = false;
		// The offset from the bottom of the page in percentage the animated element
		// has to cross to be considered 'in view'
		this.offset = offset;
		this.init();
	}

	LazyLoad.prototype.init = function() {
		// Check for support of IntersectionObserver
		// IE doesn't support it so we have to fall back
		// to the old setTimeout loop

		if ('IntersectionObserver' in window) {
			this.initIntersectionObserver();
		} else {
			this.initSetTimeout();
		}
	}


	LazyLoad.prototype.initSetTimeout = function() {
		var self = this;

		this.scrollTimer = setTimeout(this.loadImages.bind(this), 0);

		$(window).on('resize scroll show.bs.modal hide.bs.modal', function() {
			self.handleScroll();
		});
		$(document).on('ajaxSuccess ajaxError', function() {
			self.handleScroll();
		});
		$('.panel-collapse').on('show.bs.collapse', function() {
			self.handleScroll();
		});
	}

	LazyLoad.prototype.initIntersectionObserver = function() {
		var self = this;
		
		const lazyImages = document.querySelectorAll('.lazy-img');
		lazyImages.forEach(function(img) {
			// Callback which will get invoked once the image enters the viewport
			const callback = function(entries, observer) {
				// With no options provided, threshold defaults to 0 which results
				// in an array of entries storing only ONE element
				entries.forEach(function(entry) {
					if (entry.isIntersecting) {
						img = new Image();
						img.onload = function() {
							$(entry.target).attr('src', $(entry.target).data('src'));
							$(entry.target).removeClass('lazy-img');
							observer.disconnect();
						};
						img.src = $(entry.target).data('src');
					}
				});
			};

			// Use rootMargin to trigger isIntersecting callback when
			// the element crosses offset bottom of the viewport 
			const io = new IntersectionObserver(callback, {rootMargin: '-' + self.offset + '% 0% -' + self.offset + '% 0%'});
			io.observe(img);
		});
	}

	LazyLoad.prototype.loadImages = function() {
		$('.lazy-img').each(function() {
			if ($(this).isInViewport(this.offset)) {
				var el = $(this);
				img = new Image();
				img.onload = function() {
				el.attr('src', el.data('src'));
				el.removeClass('lazy-img');
				};
				img.src = el.data('src');
			}
		});
		this.scrollTimer = false;
	}

	LazyLoad.prototype.handleScroll = function() {
		var self = this;

		if(!this.scrollTimer) {
			clearTimeout(this.scrollTimer);
			this.scrollTimer = setTimeout(this.loadImages.bind(this), 300);
		}
	}

	Drupal.behaviors.lazyload = {
		attach: function (context, settings) {
			new LazyLoad(0);
		}
	};
})(jQuery, window, document, Drupal);