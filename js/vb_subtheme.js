(function($, window, document, Drupal) {
	// Prevent offcanvas FOUC
	$(window).on('load', function() {
		$('aside.right-off-canvas-menu, aside.left-off-canvas-menu').addClass('loaded');
	
		// Force is-active class on active language because
		// it doesn't always work well
		if($('.language-switcher').length) {
			var currentLanguage = $('html').attr('lang');
			$('.language-switcher .links li').each(function() {
				if($(this).attr('hreflang') == currentLanguage) {
					$(this).addClass('is-active');
					console.log($(this));
				}
			});
		}
	});

	// Language Switcher dropdown
	$(window).on('click', function(event) {
		if(!$(event.target).is($('.language-switcher--dropdown a')) && !$(event.target).children('.language-switcher--dropdown a').length) {
			$('.language-switcher--dropdown ul').removeClass('active');
			$(this).removeClass('toggled');
		} else if(!$('.language-switcher--dropdown ul').hasClass('active')) {
			event.preventDefault();
			$('.language-switcher--dropdown ul').toggleClass('active');
			$(this).toggleClass('toggled');
		}
	});

	// Ajax utility classes used for loading animation
	$(document).on({
		ajaxSend: function(event, request, settings) {
			if(settings.url.indexOf('/views/ajax') !== -1) {
				$('.view--has-ajax').addClass('loading');
			}
		},
		ajaxSuccess: function(event, request, settings) {
			if(settings.url.indexOf('/views/ajax') !== -1) {
				$('.view--has-ajax').removeClass('loading');
			}
		},
		ajaxError: function(event, request, settings) {
			if(settings.url.indexOf('/views/ajax') !== -1) {
				$('.view--has-ajax').removeClass('loading');
			}
		}
	});
})(jQuery, window, document, Drupal);
