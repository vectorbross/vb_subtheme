(function($, window, document, Drupal) {

  $.fn.isInViewport = function(offset = 0) {
    if($(this).length) {
      var elementTop = $(this).offset().top;
      var elementBottom = elementTop + $(this).outerHeight();

      var viewportTop = $(window).scrollTop();
      var viewportBottom = viewportTop + $(window).height();

      return elementBottom > viewportTop && elementTop < viewportBottom - (($(window).height() / 100) * offset);
    } else {
      return false;
    }
  };

  function Animate() {
    this.scrollTimer = false;
    this.init();
  }

  Animate.prototype.init = function() {
    // Check for support of IntersectionObserver
    // IE doesn't support it so we have to fall back
    // to the old setTimeout loop
    if (!'IntersectionObserver' in window &&
      !'IntersectionObserverEntry' in window &&
      !'intersectionRatio' in window.IntersectionObserverEntry.prototype) {

      this.initIntersectionObserver();

      // We can safely use IntersectionObserver
    } else {
      this.initSetTimeout();
    }
  }

  Animate.prototype.initSetTimeout = function() {
    var self = this;

    this.scrollTimer = setTimeout(this.animateLayoutbuilder.bind(this), 0);

    $(window).on('resize scroll show.bs.modal hide.bs.modal', function() {
      self.handleScroll();
    });
    $(document).on('ajaxSuccess ajaxError', function() {
      self.handleScroll();
    });
    $('.panel-collapse').on('show.bs.collapse', function() {
      self.handleScroll();
    });
  }

  Animate.prototype.initIntersectionObserver = function() {
    const animateLayoutbuilder = document.querySelectorAll('.layout--animated');
    animateLayoutbuilder.forEach(layout => {
      // Callback which will get invoked once the image enters the viewport
      const callback = (entries, observer) => {
        // With no options provided, threshold defaults to 0 which results
        // in an array of entries storing only ONE element
        entries.forEach(entry => {
          if (entry.isIntersecting) {
            $(entry.target).addClass('inview').removeClass('animated');
          }
        });
      };

      // Create an observer for each image
      const io = new IntersectionObserver(callback);
      io.observe(layout);
    });
  }

  Animate.prototype.animateLayoutbuilder = function() {
    $('.layout--animated .inline-block').each(function() {
      if ($(this).isInViewport(10)) {
        $(this).addClass('inview').removeClass('animated');
      }
    });

    this.scrollTimer = false;
  }

  Animate.prototype.handleScroll = function() {
    var self = this;

    if(!this.scrollTimer) {
      clearTimeout(this.scrollTimer);
      this.scrollTimer = setTimeout(this.animateLayoutbuilder.bind(this), 300);
    }
  }

  $(function() {
    new Animate;
  });

})(jQuery, window, document, Drupal);
