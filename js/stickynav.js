(function($, window, document, Drupal) {

	function StickyNav(element) {
		this.element = element;
		this.scrollBlocked = false;
		this.init();
	};


	/*
	* Initiate module
	*/
	StickyNav.prototype.init = function() {
		$('body').scrollspy({ target: '#' + this.element.attr('id') });
		this.attachHandlers();
	}


	/*
	* Attach click and scrollspy activate handler
	*/
	StickyNav.prototype.attachHandlers = function() {
		var stickyNav = this;

		stickyNav.element
			.on('click', 'a', function(event) {
				stickyNav.scrollBlocked = true;
				stickyNav.scrollToTarget($(this));
			})
			.on('click', '.next', function(event) {
				stickyNav.scrollBlocked = true;
				stickyNav.next();
			})
			.on('click', '.previous', function(event) {
				stickyNav.scrollBlocked = true;
				stickyNav.previous();
			});

		stickyNav.element.on('activate.bs.scrollspy', function(event) {
			if(!stickyNav.scrollBlocked) {
				stickyNav.scrollItemList($(event.target));
			}
		});

	}


	/*
	* Scroll to the next item of the first if there is none
	*/
	StickyNav.prototype.next = function() {
		if(this.element.find('.active').length) {
			var target = this.element.find('.active').next().find('a');
		} else {
			var target = this.element.find('li:first-child a');
		}
		this.scrollToTarget(target);
	}


	/*
	* Scroll to the previous item of the first if there is none
	*/
	StickyNav.prototype.previous = function() {
		if(this.element.find('.active').length) {
			var target = this.element.find('.active').prev().find('a');
		} else {
			var target = this.element.find('li:first-child a');
		}
		this.scrollToTarget(target);
	}


	/*
	* Scroll the active itemList child <li> to the center of the list
	* Use CSS scroll-behavior: smooth (if supported), else use jQuery animate
	*/
	StickyNav.prototype.scrollItemList = function(target) {
		if(this.element.hasClass('item-list')) {
			var list = this.element;
		} else {
			var list = this.element.find('.item-list');
		}

		if($(window).width() >= 768) {
			var autoMargin = (list.height() - target.outerHeight())/2;
			 // jQuery .offset() seems to give irregular results so we use vanilla JS
			var position = target[0].offsetTop - autoMargin;

			if('scrollBehavior' in document.documentElement.style) {
				list.scrollTop(position);
			} else {
				list.animate({scrollTop: position}, 300);
			}
		} else {
			var autoMargin = ($(window).width() - target.outerWidth())/2;
			 // jQuery .offset() seems to give irregular results so we use vanilla JS
			var position = target[0].offsetLeft - autoMargin;

			if('scrollBehavior' in document.documentElement.style) {
				list.scrollLeft(position);
			} else {
				list.animate({scrollLeft: position}, 300);
			}
		}
	}


	/*
	* Scroll page to the target element
	*/
	StickyNav.prototype.scrollToTarget = function(target) {
		var stickyNav = this;

		$('html, body').animate({ 
	        scrollTop: $(target.attr('href')).offset().top
	    }, 500, function() {
	    	stickyNav.scrollItemList(target.parent());
	    	stickyNav.scrollBlocked = false;
	    });
	}

	$(function() {
		var stickyNav = [];

		$('.sticky-nav').each(function(index) {
			stickyNav[index] = new StickyNav($(this));
		});
	});

})(jQuery, window, document, Drupal);